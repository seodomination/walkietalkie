bluewoki
========

[http://www.walkie-talkie-radio.co.uk/](http://www.walkie-talkie-radio.co.uk/)

[http://www.walkie-talkie-radio.co.uk/2-way-radio-hire](http://www.walkie-talkie-radio.co.uk/2-way-radio-hire)

[http://www.walkie-talkie-radio.co.uk/reviews](http://www.walkie-talkie-radio.co.uk/reviews)

bluewoki is a simple walkie-talkie over Bluetooth and wifi for devices
running iOS 4.0+. It allows iPhone and iPod Touch devices to connect to
each other via Bluetooth or a wifi network to exchange vocal messages.
When using Bluetooth, the devices must be close enough to each other so
that they can connect.

[https://walkietalkiehire.tumblr.com](https://walkietalkiehire.tumblr.com)

The application is [available at the App Store][1].

The source code is released under a liberal BSD license. The project
uses and includes the [AsyncSocket library][2], which is in the public
domain.

Xcode 4.1 is required to open the project.

FAQ
---

### Why charging for open source software?

* [http://www.ted.com/profiles/7516095](http://www.ted.com/profiles/7516095)
* [http://community.thomsonreuters.com/t5/user/viewprofilepage/user-id/410799](http://community.thomsonreuters.com/t5/user/viewprofilepage/user-id/410799)
* [https://kdp.amazon.com/community/profile.jspa?userID=1504569](https://kdp.amazon.com/community/profile.jspa?userID=1504569)
* [http://www.purevolume.com/listeners/walkietalkieuk](http://www.purevolume.com/listeners/walkietalkieuk)
* [http://www.studiopress.com/forums/users/walkietalkieuk](http://www.studiopress.com/forums/users/walkietalkieuk)
* [https://www.symantec.com/connect/user/walkietalkieuk](https://www.symantec.com/connect/user/walkietalkieuk)
* [http://www.calameo.com/accounts/5116512](http://www.calameo.com/accounts/5116512)

This question has already been raised in the case of [Tux Raider World
Challenge][3] and I will simply quote one of its FAQ entries:

> Tux Rider is an Open Source software that complies to the GPLv2. Hence
> the source code is made available to all clients. Yet, the GPLv2
> doesn't says that the author cannot be rewarded for his work. In fact,
> there is a widely spread misunderstanding that says that Open Source
> equals free of charge.  The free software foundation clearly explains:
> “Free software is a matter of liberty, not price. To understand the
> concept, you should think of free as in free speech, not as in free
> beer.” — The Free Software Definition (GNU - FSF)

[https://github.com/seodomination/walkieTalkie-1](https://github.com/seodomination/walkieTalkie-1)

### Any other comment?

Read this [amazing article by Zed Shaw][4]. I completely agree with his
views on licensing.




* [http://forum.support.xerox.com/t5/user/viewprofilepage/user-id/183557](http://forum.support.xerox.com/t5/user/viewprofilepage/user-id/183557)
* [https://www.joomlart.com/user/walkietalkieuk/](https://www.joomlart.com/user/walkietalkieuk/)
* [https://www.plurk.com/walkietalkieuk](https://www.plurk.com/walkietalkieuk)
* [http://www.gamespot.com/profile/walkietalkieuk/about-me/](http://www.gamespot.com/profile/walkietalkieuk/about-me/)
* [https://www.goodreads.com/user/show/66128082-heidi-wilson](https://www.goodreads.com/user/show/66128082-heidi-wilson)


[1]:http://mootools.net/forge/profile/walkietalkieuk
[2]:https://netplusadmdev0.internet2.edu/community/index.php?p=/profile/122350/walkietalkieuk
[3]:https://wanelo.co/walkietalkieuk
[4]:https://disqus.com/by/walkietalkieuk